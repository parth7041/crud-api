class User
  include ActiveModel::Validations
  attr_accessor :id, :username, :firstname, :lastname, :age

  validates :id, :username, :firstname, :lastname, :age , presence: true

  @@users = []

  def initialize(params)
    @id = @@users.count+1
    @username = params[:username]
    @firstname = params[:firstname]
    @lastname = params[:lastname]
    @age = params[:age]
  end

  def save
    if self.valid?
      @@users << self
      true
    else
      false
    end
  end

  def update(params)
    @username = params[:username] if params[:username]
    @firstname = params[:firstname] if params[:firstname]
    @lastname = params[:lastname] if params[:lastname]
    @age = params[:age] if params[:age]
    if self.valid?
      true
    else
      false
    end
  end

  def self.all
    @@users
  end

  def self.get_user(id)
    if @@users[id.to_i-1]
      @@users[id.to_i-1]
    else
      false
    end
  end

  def self.delete(id)
    if @@users[id.to_i-1]
      @@users.delete_at(id.to_i-1)
      true
    else
      false
    end
  end
end
